<?php

namespace Drupal\keepeekdam\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\keepeekdam\KeepeekdamInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller routines for Keepeek DAM routes.
 */
class KeepeekdamController extends ControllerBase {
}
